# qualite-developpement-r5-08-td

## Description

## Principes

* La branche par défaut ("main") permet de récupérer un projet maven pour commencer à travailler
* junit et log4j sont présents pour permettre l'écriture de tests unitaires et la gestion des logs

## Organisation du code

La classe [Application](src/main/java/com/iut/lannion/Application.java) est le point d'entrée de l'application. La méthode `main` assure le démarrage de l'application.

Le code est organisé en package :

* `modele` : Modélisation des données de l'application
* ̀`strategie` : Patron de conception stratégie pour les ondes
* `utils` : Package pour les classes utilitaires

## Utilisation

### Avec eclipse

* Lancer l'application "Application.java".

### En ligne de commande

* Construire le jar :

```bash
mvn clean package
```
