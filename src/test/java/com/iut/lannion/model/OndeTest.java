package com.iut.lannion.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.iut.lannion.modele.ActiviteSismique;
import com.iut.lannion.modele.Onde;
import com.iut.lannion.strategie.OndePStrategie;

/**
 * 
 * @author etassel
 */
public class OndeTest {

	@Test
	public void testPropager() throws InterruptedException {
		Onde onde = new Onde(new ActiviteSismique(6.0), new OndePStrategie());

		double test = onde.getOndeStrategy().getSpeed();
		double distance = onde.propager();
		assertTrue(distance == test);

		Thread.sleep(1000);

		test = distance + onde.getOndeStrategy().getSpeed();
		distance = onde.propager();
		assertTrue(distance == test);

		Thread.sleep(1000);

		test = distance + onde.getOndeStrategy().getSpeed();
		distance = onde.propager();
		assertTrue(distance == test);
	}
}
