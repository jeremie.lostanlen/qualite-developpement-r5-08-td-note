package com.iut.lannion.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.iut.lannion.modele.ActiviteSismique;
import com.iut.lannion.modele.Station;

/**
 * 
 * @author etassel
 */
public class StationTest {

	@Test
	public void testConstructeur() {
		Station stationFortDeFrance = new Station("Fort-de-France", 386, 240);

		assertTrue(stationFortDeFrance.getName().equals("Fort-de-France"));
		assertTrue(stationFortDeFrance.getX() == 386.0);
		assertTrue(stationFortDeFrance.getY() == 240.0);
	}

	@Test
	public void testMettreAJour() {
		Station stationFortDeFrance = new Station("Fort-de-France", 386, 240);

		ActiviteSismique activiteSismique = new ActiviteSismique(6.0);
		activiteSismique.setxFoyer(386);
		activiteSismique.setyFoyer(240);

		assertTrue(stationFortDeFrance.avertiActiviteSismique(activiteSismique));
	}

	@Test
	public void testCheckDistanceOk() {
		Station stationFortDeFrance = new Station("Fort-de-France", 386, 240);

		ActiviteSismique activiteSismique = new ActiviteSismique(6.0);
		activiteSismique.setxFoyer(386);
		activiteSismique.setyFoyer(240);

		assertTrue(stationFortDeFrance.checkDistance(activiteSismique.getxFoyer(), activiteSismique.getyFoyer(),
				activiteSismique.getOndePrimaire()));
	}

	@Test
	public void testCheckDistanceKo() {
		Station stationFortDeFrance = new Station("Fort-de-France", 386, 240);

		ActiviteSismique activiteSismique = new ActiviteSismique(6.0);
		activiteSismique.setxFoyer(0);
		activiteSismique.setyFoyer(0);

		assertFalse(stationFortDeFrance.checkDistance(activiteSismique.getxFoyer(), activiteSismique.getyFoyer(),
				activiteSismique.getOndePrimaire()));
	}
}
