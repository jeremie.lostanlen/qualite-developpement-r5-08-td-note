package com.iut.lannion.model;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

import com.iut.lannion.modele.Coordonnee;
import com.iut.lannion.modele.Station;


public class CoordonneeTest {
    @Test
    public void testCoordonneesAleatoires(){
        Coordonnee coordonneeTest = new Coordonnee();
        assertFalse(coordonneeTest.x > 600);
        assertFalse(coordonneeTest.x < 0);
        assertFalse(coordonneeTest.y > 700);
        assertFalse(coordonneeTest.y < 0);
    }
}