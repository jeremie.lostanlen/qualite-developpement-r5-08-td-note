package com.iut.lannion.strategie;

/**
 * 
 * @author etassel
 */
public interface OndeStrategie {
	double getSpeed();

	String getOndeType();
}