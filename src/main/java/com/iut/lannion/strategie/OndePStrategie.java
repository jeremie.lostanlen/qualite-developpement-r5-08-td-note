package com.iut.lannion.strategie;

/**
 * 
 * @author etassel
 */
public class OndePStrategie implements OndeStrategie {

	@Override
	public double getSpeed() {
		return 6.0; // Vitesse de l'onde P en km/s
	}

	@Override
	public String getOndeType() {
		return "ONDE_TYPE_P";
	}
}