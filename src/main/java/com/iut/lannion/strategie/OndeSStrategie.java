package com.iut.lannion.strategie;

/**
 * 
 * @author etassel
 */
public class OndeSStrategie implements OndeStrategie {

	@Override
	public double getSpeed() {
		return 3.5; // Vitesse de l'onde S en km/s
	}

	@Override
	public String getOndeType() {
		return "ONDE_TYPE_S";
	}
}