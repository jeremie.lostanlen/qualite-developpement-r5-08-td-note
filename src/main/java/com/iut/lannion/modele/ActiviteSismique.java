package com.iut.lannion.modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.iut.lannion.observateur.ActiviteSismiqueObservateur;
import com.iut.lannion.strategie.OndePStrategie;
import com.iut.lannion.strategie.OndeSStrategie;

/**
 * 
 * @author etassel
 */
public class ActiviteSismique implements Runnable, ActiviteSismiqueObservateur {
	private double magnitude;
	private double xFoyer;
	private double yFoyer;
	private Onde ondePrimaire = new Onde(this, new OndePStrategie());
	private Onde ondeSecondaire = new Onde(this, new OndeSStrategie());
	private List<Station> stations = new ArrayList<>();

	// Determines whether the waves should continue to spread or not.
	boolean running = true;

	public ActiviteSismique(double magnitude) {
		this.magnitude = magnitude;

		this.generateRandomFoyer();
	}

	private void generateRandomFoyer() {
		this.xFoyer = Math.random() * 600;
		this.yFoyer = Math.random() * 700;
	}

	@Override
	public void run() {
		System.out.println("Nouvelle activité sismique déclarée à (" + this.xFoyer + ", " + this.yFoyer + ")");
		Timer tm = new Timer();
		// Task scheduled by the timer. Used to stop the waves spread
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				running = false;
			}
		};
		// After 100 sec, the waves will stop spreading
		tm.schedule(tt, 10000); // 10 sec for tests
		while (running) {
			this.ondePrimaire.propager();
			this.ondeSecondaire.propager();

			for (Station station : stations) {
				station.avertiActiviteSismique(this);
			}

			try {
				Thread.sleep(1000); // Waves spread every seconds
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// Stop the thread after the wave finished to spread
		tm.cancel();
	}

	public void arreter() {
		this.running = false;
	}

	public void ajouterStations(List<Station> stations) {
		this.stations.addAll(stations);
	}

	public void supprimerStation(Station station) {
		this.stations.remove(station);
	}

	public double getxFoyer() {
		return xFoyer;
	}

	public void setxFoyer(double xFoyer) {
		this.xFoyer = xFoyer;
	}

	public double getyFoyer() {
		return yFoyer;
	}

	public void setyFoyer(double yFoyer) {
		this.yFoyer = yFoyer;
	}

	public Onde getOndePrimaire() {
		return ondePrimaire;
	}

	public void setOndePrimaire(Onde ondeP) {
		this.ondePrimaire = ondePrimaire;
	}

	public Onde getOndeSecondaire() {
		return ondeSecondaire;
	}

	public void setOndeSecondaire(Onde ondeS) {
		this.ondeSecondaire = ondeSecondaire;
	}

	public double getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(double magnitude) {
		this.magnitude = magnitude;
	}

	public List<Station> getStations() {
		return stations;
	}

	public void setStations(List<Station> stations) {
		this.stations = stations;
	}

	// Patron de conception Observateur 
	public void attacheStation(Station station){
		this.stations.add(station);
	}
	
	public void detacheStation(int stationNum){
		this.stations.remove(stationNum);
	}

	public void notifierStation(){
		for (Station station : stations) {
			station.averti(this);
		}
	}

}
