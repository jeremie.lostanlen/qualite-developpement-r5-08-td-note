package com.iut.lannion.modele;

import java.util.ArrayList;
import java.util.List;

import com.iut.lannion.observateur.StationObservateur;

/**
 * 
 * @author etassel
 */
public class Station implements StationObservateur{
	private String name;
	private double xPosition;
	private double yPosition;
	private List<ActiviteSismique> activiteSismiques = new ArrayList<>();

	public Station(String name, double xPosition, double yPosition) {
		this.name = name;
		this.xPosition = xPosition;
		this.yPosition = yPosition;
	}

	public boolean avertiActiviteSismique(ActiviteSismique activiteSismique) {
		if (this.checkDistance(activiteSismique.getxFoyer(), activiteSismique.getyFoyer(),
				activiteSismique.getOndePrimaire())) {
			this.declencherAlarme(activiteSismique.getxFoyer(), activiteSismique.getyFoyer(),
					activiteSismique.getOndePrimaire());
			this.enregistrerActiviteSismique(activiteSismique);
			return true;

		}
		if (this.checkDistance(activiteSismique.getxFoyer(), activiteSismique.getyFoyer(),
				activiteSismique.getOndeSecondaire())) {
			this.declencherAlarme(activiteSismique.getxFoyer(), activiteSismique.getyFoyer(),
					activiteSismique.getOndePrimaire());
			this.enregistrerActiviteSismique(activiteSismique);
			return true;
		}

		return false;
	}

	public void declencherAlarme(double xFoyer, double yFoyer, Onde onde) {
		double distStationFoyer = this.getDistance(xFoyer, yFoyer, onde);
		System.out.println("************************* ALERTE ACTIVITE SISMIQUE " + this.getName()
				+ " *******************************");
		System.out.println("Une activité sismique a été détectée à la position (" + this.xPosition + ", "
				+ this.yPosition + ") - distance : " + onde.getDistanceActuelle() + " km - Vitesse : "
				+ onde.getVitesseActuelle() + " km/s");
		System.out.println("L'activité sismique se situe à " + distStationFoyer + " km");
		System.out.println("**********************************************************************************");

	}

	public void enregistrerActiviteSismique(ActiviteSismique activiteSismique) {
		if (!this.activiteSismiques.contains(activiteSismique)) {
			this.activiteSismiques.add(activiteSismique);
		}
	}

	public void listeActiviteSismique() {
		System.out.println("************************* LISTE DES ACTIVITES SISMIQUES " + this.getName()
				+ " *******************************");
		if (this.activiteSismiques.isEmpty()) {
			System.out.println("Aucune activité enregistrée");
		} else {
			System.out.println("Nombre enregistrement : " + this.activiteSismiques.size());
			for (ActiviteSismique activiteSismique : this.activiteSismiques) {
				System.out.println("Activité sismique déclarée de magnitude " + activiteSismique.getMagnitude() + " à ("
						+ activiteSismique.getxFoyer() + ", " + activiteSismique.getxFoyer() + ")");
			}
		}
	}

	public boolean checkDistance(double xFoyer, double yFoyer, Onde onde) {
		return this.getDistance(xFoyer, yFoyer, onde) < 10;
	}

	public double getDistance(double xFoyer, double yFoyer, Onde onde) {
		double distStationFoyer = Math
				.sqrt(Math.pow(xFoyer - this.xPosition, 2) + Math.pow(yFoyer - this.yPosition, 2));

		return Math.abs(distStationFoyer - onde.getDistanceActuelle());
	}

	public String getName() {
		return name;
	}

	public double getX() {
		return xPosition;
	}

	public double getY() {
		return yPosition;
	}

	public boolean averti(ActiviteSismique activiteSismique){
		return true;
	}
}
