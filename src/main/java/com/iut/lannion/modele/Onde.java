package com.iut.lannion.modele;

import com.iut.lannion.strategie.OndeStrategie;

/**
 * 
 * @author etassel
 */
public class Onde {
	private ActiviteSismique activiteSismique;
	private OndeStrategie ondeStrategy;
	private double distanceActuelle = 0.0;

	public Onde(ActiviteSismique activiteSismique) {
		this.activiteSismique = activiteSismique;
	}

	public Onde(ActiviteSismique activiteSismique, OndeStrategie ondeStrategy) {
		this.activiteSismique = activiteSismique;
		this.ondeStrategy = ondeStrategy;
	}

	public double propager() {
		double vitesse = ondeStrategy.getSpeed();

		// Calcul de la nouvelle distance actuelle en fonction de la vitesse
		distanceActuelle += vitesse;

		System.out.println("Onde " + this.getOndeStrategy().getOndeType() + " propagée depuis ("
				+ activiteSismique.getxFoyer() + ", " + activiteSismique.getyFoyer() + ") avec une vitesse de "
				+ vitesse + " km/s, distance actuelle: " + distanceActuelle + " km");

		return this.distanceActuelle;
	}

	public double getDistanceActuelle() {
		return distanceActuelle;
	}

	public double getVitesseActuelle() {
		if (null != ondeStrategy)
			return ondeStrategy.getSpeed();
		else
			return 0;
	}

	public ActiviteSismique getActiviteSismique() {
		return activiteSismique;
	}

	public void setActiviteSismique(ActiviteSismique activiteSismique) {
		this.activiteSismique = activiteSismique;
	}

	public OndeStrategie getOndeStrategy() {
		return ondeStrategy;
	}

	public void setOndeStrategy(OndeStrategie ondeStrategy) {
		this.ondeStrategy = ondeStrategy;
	}

	public void setDistanceActuelle(double distanceActuelle) {
		this.distanceActuelle = distanceActuelle;
	}

}
