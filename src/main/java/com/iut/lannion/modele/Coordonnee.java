package com.iut.lannion.modele;

public class Coordonnee {
    public double x;
    public double y;

    // Constructeur 1 avec paramètres 
    public Coordonnee(double x, double y){
        this.x = x;
        this.y = y;
    }

    // Constructeur 2 qui génère des foyers aléatoires
    public Coordonnee(){
        this.x = Math.random() * 600;
        this.y = Math.random() * 700;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public void setX(double newX){
        this.x = newX;
    }

    public void setY(double newY){
        this.y = newY;
    }
}
