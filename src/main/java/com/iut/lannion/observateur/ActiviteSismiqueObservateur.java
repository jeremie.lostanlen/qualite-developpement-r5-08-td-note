package com.iut.lannion.observateur;

import com.iut.lannion.modele.Station;

public interface ActiviteSismiqueObservateur {
    public void attacheStation(Station observateur);
    public void detacheStation(int observateurNum); 
    public void notifierStation();
}
