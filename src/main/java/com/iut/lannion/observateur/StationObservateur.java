package com.iut.lannion.observateur;

import com.iut.lannion.modele.ActiviteSismique;

public interface StationObservateur {
    boolean averti(ActiviteSismique activiteSismique);
}
