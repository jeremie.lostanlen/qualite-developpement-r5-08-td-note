package com.iut.lannion;

import java.util.ArrayList;
import java.util.List;

import com.iut.lannion.modele.ActiviteSismique;
import com.iut.lannion.modele.Station;

/**
 * Classe du programme principal de l'application
 * 
 * @author etassel
 */
public class Application {

	public static void main(String[] args) {
		List<Station> stations = new ArrayList<>();
		Station station1 = new Station("Fort-de-France", 386, 240);
		Station station2 = new Station("Le Robert", 292, 431);
		Station station3 = new Station("Ducos", 432, 389);
		Station station4 = new Station("Le Morne-Rouge", 129, 133);
		Station station5 = new Station("Le Lorrain", 71, 248);
		Station station6 = new Station("Les Trois-Îlets", 504, 259);
		Station station7 = new Station("Le Diamant", 580, 334);
		Station station8 = new Station("Le Vauclin", 502, 570);
		stations.add(station1);
		stations.add(station2);
		stations.add(station3);
		stations.add(station4);
		stations.add(station5);
		stations.add(station6);
		stations.add(station7);
		stations.add(station8);

		List<ActiviteSismique> activiteSismiques = new ArrayList<>();

		// On simule un monde ou une activité sismique est déclarée dans le monde toutes
		// les secondes
		for (int i = 0; i < 10; i++) {
			ActiviteSismique activiteSismique = new ActiviteSismique(6.0); // Magnitude fictive pour le test
			activiteSismiques.add(activiteSismique);

			activiteSismique.ajouterStations(stations);

			Thread activiteThread = new Thread(activiteSismique);
			activiteThread.start();

			// Laissez le thread de l'activité sismique s'exécuter pendant un certain temps
			try {
				Thread.sleep((long) (Math.random() * 12000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// Arrêtez l'activité sismique après un certain temps (c'est facultatif)
		for (ActiviteSismique activiteSismique : activiteSismiques) {
			activiteSismique.arreter();
		}

		// Liste des activités sisimique enregistrée durnt le test
		for (Station station : stations) {
			station.listeActiviteSismique();
		}
		// Le programme principal se termine ici
	}
}